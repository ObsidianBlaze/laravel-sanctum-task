<?php

namespace Tests\Feature\Http\Controllers;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    use RefreshDatabase;

    public function test_login_with_valid_credentials_returns_access_token()
    {
        $password = 'password';
        $user = factory(User::class)->create([
            'password' => Hash::make($password),
        ]);

        $response = $this->postJson('/api/v1/login', [
            'email' => $user->email,
            'password' => $password,
        ]);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'access_token',
                'token_type',
            ]);
    }

    public function test_login_with_invalid_credentials_returns_unauthorized()
    {
        $password = 'password';
        $user = factory(User::class)->create([
            'password' => Hash::make($password),
        ]);

        $response = $this->json('POST', '/api/v1/login', [
            'email' => $user->email,
            'password' => 'invalid_password',
        ]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson([
                'message' => 'Unauthorized',
            ]);
    }
}
