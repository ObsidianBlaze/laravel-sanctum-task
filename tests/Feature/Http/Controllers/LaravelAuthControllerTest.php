<?php

namespace Tests\Feature\Http\Controllers;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class LaravelAuthControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @test
     * @return void
     */
    public function it_can_test_user_can_get_post()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/api/v1/posts');
        $response->assertStatus(200);
    }

    /**
     * @test
     * @return void
     */
    public function normal_auth_can_create_post()
    {
        $user = factory(User::class)->create();
        $data = [
            'title' => $this->faker->sentence(),
        ];

        $this->actingAs($user);

        $response = $this->json('POST', '/api/v1/posts', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'message',
                'post' => [
                    'id',
                    'user_id',
                    'title',
                ],
            ])
            ->assertJson([
                'message' => 'Post created successfully!',
            ]);
    }
}
