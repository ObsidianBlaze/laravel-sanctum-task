<?php

namespace Tests\Feature\Http\Controllers;

use App\Http\Resources\PostResource;
use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private $user;
    private $token;

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        $this->user = factory(User::class)->create();
        $this->token = $this->user->createToken('test-token', ['post:all', 'post:detail', 'post:delete'])->plainTextToken;
    }

    /**
     * @test
     * @return void
     */
    public function it_should_return_all_posts()
    {
        Sanctum::actingAs($this->user, ['post:all']);

        $posts = factory(Post::class, 3)->create();

        $response = $this->get('/api/v1/posts');

        $response->assertOk();
    }

    /**
     * @test
     * @return void
     */
    public function it_should_not_return_all_posts_without_permissions()
    {
        Sanctum::actingAs($this->user);

        $response = $this->get('/api/v1/posts');

        $response->assertForbidden();
    }

    /**
     * @test
     * @return void
     */
    public function it_should_not_show_a_post_without_permissions()
    {
        $post = factory(Post::class)->create(['user_id' => $this->user->id]);

        Sanctum::actingAs($this->user);

        $response = $this->getJson('/api/v1/posts/' . $post->id);

        $response->assertForbidden();
    }

    /**
     * @test
     * @return void
     */
    public function it_should_show_my_posts()
    {
        $posts = factory(Post::class, 3)->create(['user_id' => $this->user->id]);

        Sanctum::actingAs($this->user);

        $response = $this->getJson('/api/v1/posts/me');

        $response->assertOk();
    }

    /**
     * @test
     * @return void
     */
    public function it_should_delete_a_post()
    {
        $post = factory(Post::class)->create(['user_id' => $this->user->id]);

        Sanctum::actingAs($this->user, ['post:delete']);

        $response = $this->deleteJson('/api/v1/posts/' . $post->id);

        $response->assertOk();

        $this->assertDatabaseMissing('posts', ['id' => $post->id]);
    }

    /**
     * @test
     * @return void
     */
    public function test_user_can_create_post()
    {
        $user = factory(User::class)->create();
        $data = [
            'title' => $this->faker->sentence(),
        ];

         Sanctum::actingAs($user);

        $response = $this->json('POST', '/api/v1/posts', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'message',
                'post' => [
                    'id',
                    'user_id',
                    'title',
                ],
            ])
            ->assertJson([
                'message' => 'Post created successfully!',
            ]);
    }

    /**
     * @test
     * @return void
     */
public function it_delete_stuff(){

    // Generate an API token using Sanctum
    Sanctum::actingAs($this->user, ['post:delete']);

    // Create a post owned by the authenticated user
    $post = factory(Post::class)->create(['user_id' => $this->user->id]);

//    dd($post);

    // Make a DELETE request to the API endpoint
    $response = $this->delete('/api/v1/posts/'.$post->id);

    // Assert that the response status code is 200 and the message 'Post deleted.' is returned
    $response->assertStatus(200)->assertJson(['message' => 'Post deleted.']);

    // Assert that the post has been deleted from the database
    $this->assertDatabaseMissing('posts', ['id' => $post->id]);

}
}
