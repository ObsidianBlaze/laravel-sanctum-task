<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    public function index()
    {
        if (Auth::user()->tokenCan('post:all')) {
            return PostResource::collection(Post::all());
        }
        abort(Response::HTTP_FORBIDDEN);
    }


    public function store(StorePostRequest $request)
    {
        $post = new Post([
            'title' => $request->title,
            'user_id' => Auth::id(),
        ]);
        $post->save();

        return response()->json([
            'message' => 'Post created successfully!',
            'post' => new PostResource($post),
        ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);
        if (Auth::user()->tokenCan('post:detail') && Auth::id() === $post->user_id) {
            return new PostResource($post);
        }
        abort(Response::HTTP_FORBIDDEN);
    }

    public function me(Request $request)
    {
        $posts = Post::where('user_id', $request->user()->id)->get();

        return response()->json($posts);
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        if (Auth::user()->tokenCan('post:delete') && Auth::id() == $post->user_id) {
            $post->delete();
            return response()->json(['message' => 'Post deleted.']);
        }
        abort(Response::HTTP_FORBIDDEN);
    }
}
