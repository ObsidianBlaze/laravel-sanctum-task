<?php

namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiRegisterRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{
    public function register(ApiRegisterRequest $request)
    {
        // Create new user
        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);
        $user->save();

        $token = $user->createToken('token', [
            'post:create',
            'post:all',
            'post:detail',
            'post:delete'
        ]);

        // Return response
        return response()->json([
            'token' => $token->plainTextToken,
            'user' => $user
        ], Response::HTTP_CREATED);
    }
}
